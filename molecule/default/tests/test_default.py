import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_wlcg_repo_is_installed(host):
    os_rel_maj_vers = host.system_info.release.split('.')[0]
    if os_rel_maj_vers == '6':
        assert host.package('HEP_OSlibs_SL6').is_installed
    else:
        assert host.package('HEP_OSlibs').is_installed
