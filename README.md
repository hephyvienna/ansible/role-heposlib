Ansible Role heposlib
=====================

Install HEP dependencies 

[Details](https://gitlab.cern.ch/linuxsupport/rpms/HEP_OSlibs)

Requirements
------------

* EL6/7
* EPEL repository
* WLCG repository

Role Variables
--------------

Name of the packaages to be installed

    heposlib_pkgs: name of the package



Example Playbook
----------------

    - hosts: servers
      roles:
         - geerlingguy.repo-epel
         - hephyvienna.repo-wlcg
         - hephyvienna.heposlib

License
-------

MIT

Author Information
------------------

Written by [Dietrich Liko](Dietrich.Liko@oeaw.ac.at) in March 2019

[Institute for High Energy Physics](http://www.hephy.at) of the
[Austrian Academy of Sciences](http://www.oeaw.ac.at)
